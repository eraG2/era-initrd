# era-initrd

> ERA Project initramfs framework

Complete framework for building an initramfs, made for the ERA Gentoo/GNU/Linux Project

Depends on app-misc/pax-utils[python]

---

When the initramfs phase of bootup is entered, the file /init is executed.
By default, this /init mounts needed kernel-API filesystems and executes a series of "modes" specified on the kernel command line.
If no mode is specified, a default mode of "rescue" is entered, in which it is possible to manually mount and switch_root() into the root filesystem.

All that's needed to make an ERA initrd is to write a mode script and run `make update build buildspeedy install` from the project root directory.

---

The bare minimum initialization mode must simply mount the root filesystem at /realroot.
In a default ERA install, the autodetect script probes what hardware is in use, and runs a given series of modes based on that information.

---

*Files:*

/commonFunctions.hsh - common shell functions

/init - first userspace program executed by the kernel on system boot

/keys - directory into which the user's encryption keys, presumed to reside at /admin/keys, are bound-mount into during compilation

/Makefile - standard Makefile

/Manifest - manifest of executable which should be bundled into the initramfs

/modes - scripts executed by /init

/realroot - directory onto which the real root filesystem is mounted

/scripts - scripts used during compilation of the initramfs
