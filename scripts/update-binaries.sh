#!/usr/bin/env sh
set -x
set -e

for bin in $(cat Manifest); do
	lddtree --copy-to-tree=. $bin
done
