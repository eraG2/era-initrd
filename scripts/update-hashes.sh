#!/usr/bin/env sh
set -x
for BIN in $(cat modes/roothash/Manifest); do
	sha512sum $BIN >> modes/roothash/hashes
done
