#!/usr/bin/env sh
. /commonFunctions.hsh

for BIN in $(cat /modes/roothash/Manifest); do
	sha512sum $BIN >> /hashes.new
done

diff /hashes.new /modes/roothash/hashes &&
	echo "System binary hashes look good..." ||
	(echo "System binaries failed hash recognition!"; abortsh "Unrecognized binaries on system, please take manual intervention")
