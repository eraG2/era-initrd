KVER:=`uname -m`

build:
	mkdir -p lib/modules || true; mkdir -p lib/firmware || true
	mount --bind ../../../admin/keys keys
	mount --bind ../../../lib/modules lib/modules
	mount --bind ../../../lib/firmware lib/firmware
	find . -not -name "initrd*" | cpio --quiet -H newc -o | gzip -9 -n > initrd.cpio.gz9-${KVER}
	umount keys
	umount lib/modules
	umount lib/firmware

buildspeedy:
	mount --bind ../../../admin/keys keys
	find . -not -name "initrd*" | cpio --quiet -H newc -o | gzip -9 -n > initrd.speedy.cpio.gz9-${KVER}
	umount keys

install:
	mv initrd*.cpio.gz9* /boot

update:
	rm -r bin sbin usr || true
	scripts/update-binaries.sh
	scripts/update-hashes.sh

clean:
	rm -r bin sbin usr || true
	rm -r lib* || true

distclean: clean
	rm -r etc || true
	rm -r dev/** proc/** run/** sys/** tmp/** || true
